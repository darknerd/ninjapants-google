# **Google Cloud**

Welcome.  These are my experiments with **Infrastructure as Code** for Google Cloud Platform.

- Joaquín Menchaca (2018年05月12日)

## **Getting Started**

To get started with Google Cloud, you will need to do the following:

1. Create a new [billing account](https://cloud.google.com/billing/docs/how-to/manage-billing-account):
   * create google account if you do have one already
   * enable billing for that account (free trial)
1. Create a Google Project (or use default)
1. Enable [billing for that project](https://cloud.google.com/billing/docs/how-to/modify-project):
1. Install [Google Cloud SDK](https://cloud.google.com/sdk/)
1. Authorize gcloud tools
   * https://cloud.google.com/compute/docs/gcloud-compute/

I wrote some blogs on juggling multiple google accounts or getting started with Chef and GCP:

* [Juggling Accounts with GCloud SDK](https://medium.com/@Joachim8675309/juggling-accounts-with-gcloud-sdk-f12f282a439c)
* [Starting with Chef on GCP](https://medium.com/@Joachim8675309/starting-with-chef-on-gcp-b1a33a721c17)


### **Ubuntu Install Script**

You can install Ubuntu using shell script:

```bash
sudo ./gcloud_ubuntu_install.sh
```

Or you can use the supplied Chef recipe (assumes ChefDK is installed):

```bash
sudo chef exec chef-apply gcloud_ubuntu_recipe.rb
```

## **Generate SSH Key Pair**

Generate an SSH key pair that will use deployments and configuration of GCE instances.  Install this key for the `ubuntu` user into your project's metadata.  A sample script is provided to demonstrate how to do this:

```bash
# set GCP_PROJECT using current configured project
export GCP_PROJECT=$(gcloud config list --format 'value(core.project)')
./gce-genkey.sh # generate and install GCP SSH Keys for current project
```

## **Provision Systems**

Provision a system with one of these tools:

* [gcloud command line](gcloud/README.md)
* [terraform](terraform/README.md)

These are experimental:

* [Knife Google Plugin](knife-google/README.md) - Extends `knife` tool to support creation of GCE instances and then subsequently bootstrap them.
* [Chef Chef GCompute Cookbook](chef-gcompute/README.md) - Google's official Chef Cookbook

## **Gathering GCE Instance Info**

```bash
# list gce instances for current project
gcloud compute instances list
# list metadata (yaml) for given gce instance
gcloud compute instances describe ${GCP_INSTANCE_NAME}
```

## **Logging into GCE Instance**

After this, you can log into these systems with:

```bash
ssh -i ~/.ssh/gce.key ubuntu@$CONFIG_IP
```

The `CONFIG_IP` is the IP address used to configure the system.  These scripts use a public IP address that is used for configuration of GCE systems.  This is typical style for test or staging environments.  For production, this is not recommended.

## **BootStrap Systems with Chef**

This is a script that can boostrap the systems using `knife bootstrap` provided the there's an `../.config` configuration environment.  

```bash
./gce-bootstrap.sh
```


**Note**: if you used these public IP address to provision a system and have auth errors, you may need to purge the entry in `known_hosts` before running bootstrap scripts again.

```bash
ssh-keygen -f ~/.ssh/known_hosts -R $EXT_IPADDRESS
```
