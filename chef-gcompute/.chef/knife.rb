chef_repo = File.join(File.dirname(__FILE__), '..')
infra_cookbooks = "#{chef_repo}/cookbooks"
berks_cookbooks = "#{chef_repo}/berks-cookbooks"
cookbook_path [infra_cookbooks,berks_cookbooks]
local_mode true
chef_zero.enabled true
local_level :info
cookbook_license 'mit'
