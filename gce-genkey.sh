#!/usr/bin/env bash
#
# The MIT License (MIT)
#
# Copyright:: 2018, Joaquin Menchaca
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

[[ -z "${GCP_PROJECT}" ]] && { echo 'Error: GCP_PROJECT not set. Exiting' 1>&2; exit 1; }

KEYPATH=".sekrets"
GCE_KEYS="${KEYPATH}/${GCP_PROJECT}.ssh-keys"

# Generate Key Pair
ssh-keygen -t rsa -b 4096 -f "${KEYPATH}/gce.key" -C ubuntu -q -N ""

# Create Google Style ssh-key dsv (colon sep file)
printf '%s:%s\n' 'ubuntu' "$(cat ${KEYPATH}/gce.key.pub)" >> ${GCE_KEYS}

# Install Keys into Metadata
gcloud compute project-info add-metadata \
  --metadata-from-file ssh-keys=${GCE_KEYS}

# Backup Key to ~/.ssh/
cp ${KEYPATH}/gce.key ${HOME}/.ssh
