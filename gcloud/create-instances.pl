#!/usr/bin/env perl
#
# The MIT License (MIT)
#
# Copyright:: 2018, Joaquin Menchaca
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

use strict;
use JSON::MaybeXS qw(encode_json decode_json);

############################################################
# Validate requirements
############################################################
my $gcp_project = $ENV{'GCP_PROJECT'} or die 'GCP_PROJECT not set!!';
my $gcp_service_acct = $ENV{'GCP_SERVICE_ACCT'} or die 'GCP_SERVICE_ACCT not set!!';
my $config_file = $ENV{'CONFIG_DATA'} || "gce.json";
my $config_json = do { local(@ARGV, $/) = $config_file; <> }; # file to str

############################################################
# Create data structure from JSON (GCE version)
############################################################
my $configs = decode_json $config_json;

############################################################
# Create systems (instances) using data structure
############################################################
foreach my $system (keys %$configs) {
    my %config = %{$configs->{$system}}; # deref to local hash
    my $scopes = join ',', @{$config{'scopes'}};
    my $tags = join ',', @{$config{'tags'}};
    my $tags_options = $tags ne "" ? "--tags=$tags" : "";

    my $gloud_command = <<"EOT_COMPUTE";
gcloud compute --project=$gcp_project instances create $system \\
  --zone=$config{'zone'} \\
  --machine-type=$config{'machine-type'} \\
  --subnet=default \\
  --maintenance-policy=MIGRATE \\
  --service-account=$gcp_service_acct \\
  --scopes=$scopes \\
  $tags_options \\
  --image=$config{'image'} \\
  --image-project=$config{'image-project'} \\
  --boot-disk-size=$config{'boot-disk-size'} \\
  --boot-disk-type=$config{'boot-disk-type'} \\
  --boot-disk-device-name=$system
EOT_COMPUTE
    system "$gloud_command\n";

    # convert hash to array of "key=value" strings
    my @metadata = map { "$_=$config{'metadata'}->{$_}" } keys %{$config{'metadata'}};
    # add metadata on the instance
    foreach my $data (@metadata) {
      # build commands
      my $status_cmd = <<"EOT_STATUS";
gcloud compute instances list \\
  --filter="name=$system AND zone ~ $config{'zone'}" \\
  --format='table(name,status)'
EOT_STATUS
      my $add_metadata_cmd = <<"EOT_METADATA";
gcloud compute instances add-metadata $system \\
  --zone=$config{'zone'} \\
  --metadata $data"
EOT_METADATA
      # get status of the instance
      my $status =
        (split /\s+/,                                 # split 1 plus spaces
         (grep { chomp; /$system/ } `$status_cmd`)[0] # get valid match
        )[-1];                                        # grab column of status
      # add metadata on the instance if it is running
      system "$add_metadata_cmd\n" if $status =~ /RUNNING/;
    }
}
