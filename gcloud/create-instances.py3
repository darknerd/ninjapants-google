#!/usr/bin/env python3
#
# The MIT License (MIT)
#
# Copyright:: 2018, Joaquin Menchaca
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import os
import subprocess
import re

try:
    import json
except:
    import simplejson as json

gcp_project = os.environ['GCP_PROJECT'] or Exception("GCP_PROJECT not set!!").throw()
gcp_service_acct = os.environ['GCP_SERVICE_ACCT']  or Exception("GCP_SERVICE_ACCT not set!!").throw()
config_file = os.environ.get('CONFIG_DATA') or 'gce.json'

configs = json.loads(open(config_file).read())

for system, config in configs.items():
    # reformat lists into comma-separated string
    scopes = ','.join(config['scopes'])
    tags = ','.join(config['tags'])

    # set tag options to empty string if there are no tags
    tags_options = '' if tags == '' else "--tags={}".format(tags)

    # create formatted gcloud command
    gcloud_command = f"""gcloud compute --project={gcp_project} instances create {system} \
  --zone={config['zone']} \
  --machine-type={config['machine-type']} \
  --subnet=default \
  --maintenance-policy=MIGRATE \
  --service-account={gcp_service_acct} \
  --scopes={scopes} \
  {tags_options} \
  --image={config['image']} \
  --image-project={config['image-project']} \
  --boot-disk-size={config['boot-disk-size']} \
  --boot-disk-type={config['boot-disk-type']} \
  --boot-disk-device-name={system}
    """

    process = subprocess.Popen(gcloud_command, stdout=subprocess.PIPE, shell=True)

    for metakey, metavalue in config['metadata'].items():
        # foormat status command
        status_cmd = f"""gcloud compute instances list \
          --filter="name={system} AND zone ~ {config['zone']}" \
          --format='table(name,status)'
        """

        # run status command to fetch status
        process = subprocess.Popen(status_cmd, stdout=subprocess.PIPE, shell=True)
        (output, err) = process.communicate()
        if not output: break
        # extract status from output
        print(output)
        status = list(filter(lambda line: system in line, output.split("\n")))[0].split()[-1]

        if re.match('RUNNING', status):
            add_metadata_cmd = f"""gcloud compute instances add-metadata {system} \
              --zone={config['zone']} \
              --metadata "{metakey}={metavalue}"\n"""
            subprocess.Popen(add_metadata_cmd, stdout=subprocess.PIPE, shell=True)
