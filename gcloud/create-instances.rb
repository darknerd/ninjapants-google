#!/usr/bin/env ruby
#
# The MIT License (MIT)
#
# Copyright:: 2018, Joaquin Menchaca
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
require 'json'

gcp_project = ENV['GCP_PROJECT'] || (puts "GCP_PROJECT not set!!"; exit status 1)
gcp_service_acct = ENV['GCP_SERVICE_ACCT'] || (puts "GCP_SERVICE_ACCT not set!!"; exit status 1)

config_file = ENV['CONFIG_DATA'] || 'gce.json'
config_json = open(config_file).read # read json file into string
configs = JSON.parse config_json # convert raw json string into ruby hash

configs.each do |system, config|
  scopes = config['scopes'].join ','
  tags_options = config['tags'].empty? ? '' : "--tags=#{config['tags'].join ','}"

  gcloud_command = <<~EOT_COMPUTE
    gcloud compute --project=#{gcp_project} instances create #{system} \\
      --zone=#{config['zone']} \\
      --machine-type=#{config['machine-type']} \\
      --subnet=default \\
      --maintenance-policy=MIGRATE \\
      --service-account=#{gcp_service_acct} \\
      --scopes=#{scopes} \\
      #{tags_options} \\
      --image=#{config['image']} \\
      --image-project=#{config['image-project']} \\
      --boot-disk-size=#{config['boot-disk-size']} \\
      --boot-disk-type=#{config['boot-disk-type']} \\
      --boot-disk-device-name=#{system}
  EOT_COMPUTE

  system "#{gcloud_command}\n"

  config['metadata'].each do |metakey,metavalue|
    status = %x(
      gcloud compute instances list \\
       --filter="name=#{system} AND zone ~ #{config['zone']}" \\
       --format='table(name,status)'
     ).split(/\n/)           # seperate str lines into list
      .select { |line|
        line =~ /#{system}/  # grab line that matches
      }[0]
      .split(/\s+/)[-1]      # grab only status column

    # add metadata only on running systems
    system "gcloud compute instances add-metadata #{system} \\
      --zone=#{config['zone']} \\
      --metadata #{metakey}=#{metavalue}\n\n" if status =~ /RUNNING/
  end
end
