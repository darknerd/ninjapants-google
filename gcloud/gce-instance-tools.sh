#!/usr/bin/env bash
#
# The MIT License (MIT)
#
# Copyright:: 2018, Joaquin Menchaca
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

[[ -z "${GCP_SERVICE_ACCT}" ]] && { echo 'Error: GCP_SERVICE_ACCT not set. Exiting' 1>&2; exit 1; }

SCOPES=(devstorage.read_only
       logging.write
       monitoring.write
       pubsub
       servicecontrol
       service.management.readonly
       trace.append
       userinfo.email
       compute.readonly)
PREFIX='https://www.googleapis.com/auth'
SCOPE_URLS=$(IFS=, ; echo "${SCOPES[*]/#/$PREFIX/}")

### create tools system
GCP_INSTANCE_NAME='dk-prod-tools'

gcloud compute --project="${GCP_PROJECT}" instances create "${GCP_INSTANCE_NAME}" \
  --zone='us-east1-b' \
  --machine-type='n1-standard-1' \
  --subnet='default' \
  --maintenance-policy='MIGRATE' \
  --service-account="${GCP_SERVICE_ACCT}" \
  --scopes="${SCOPE_URLS}" \
  --min-cpu-platform='Automatic' \
  --tags=elasticsearch \
  --image='ubuntu-1404-trusty-v20180308' \
  --image-project='ubuntu-os-cloud' \
  --boot-disk-size='10GB' \
  --boot-disk-type='pd-standard' \
  --boot-disk-device-name="${GCP_INSTANCE_NAME}"

# Enable Project-Wide SSH Key (if not enabled already)
gcloud compute instances add-metadata "${GCP_INSTANCE_NAME}" \
  --metadata block-project-ssh-keys='FALSE'
