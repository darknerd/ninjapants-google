# **Knife Google Tool**

This is exploration of the [knife-google](https://github.com/chef/knife-google) plug-in on April 18, 2018.

##  **Overview**


### **Provisioning Layer**

Knife tool crosses multiple layers: *infrastructure layers* and *system/application configuration layers*.

| Layer                          | Tool                               | Examples  |
|--------------------------------|------------------------------------|-----------|
| configuration: application     | knife-google (bootstrap) or other  | wordpress |
| configuration: system          | knife-google (boottrap)            | /etc/hosts|
| configuration: base provision  | knife-google (boostrap)            | chef agent|
| infrastructure (instances)     | knife-google (create)              | gce       |
| infrastructure (net, firewall) | gcloud, google console             | vpc       |

Personally, I think this is an anti-pattern for installing a full services or application using orchestration pattern with Knife.  Infrastructure resource provisioning should happen on a separate layer, and use Infrastructure as Code principals.

For base-lining, meaning installing agents, monitoring, security policy, and other system-wide configurations that are applied to all systems -- creating a base state -- this can be acceptable.  Later a secondary provisioning phase takes place.

In Chef, this (adding a secondary provisioning later) is particularly problematic, as nodes in Chef need to be configured manually (text editor) and one system at a time.  Thus the only opportunity for full automation is at bootstrap level.  Knife-Tool thus is an anti-pattern to fix an anti-pattern in this particular case.

## **Utility of Solution**

The reason for this tool as a single tool to provision infrastructure and configure/install software on the systems configured.  The problem occurs in any layer, you have to destructively recreate start from scratch, or do some manual changes.  For this reason, personally, I prefer to use gcloud SDK (or terraform or ansible) for provisioning, then use regular `knife boostrap`.

There are some use cases where this tool will simply not work, because you need the state of live running systems before applying a chef recipe:

### **Statically Configured Cluster**

For a cluster that needs to be configured with the IP addresses of other nodes, such as an ElasticSearch cluster, this solution will **NOT** work.

The desired systems in the cluster must be running to be able to use their IP addresses for configuring other solutions, as `knife google` will both create the instances and bootstrap them, they cannot know about the state of the other systems.

### **Systems Configured by Tags or Service Discovery**

Systems with service discovery (consul, etcd, zookeeper, etc.), then this solution would work, as their state is dependent on a solution outside of Chef.

As this solution also supports setting scope with `--gce-service-account-scopes`, using Google tags for service discovery will also work.

## **Prerequisites**

Before you begin using this tool, you must have the following:

1. GoogleCloud Account, authorized credentials to create systems
1. ChefServer Account, authorized credentials to create nodes
1. Installed public SSH Keys into project
1. Access to corresponding private SSH Key for bootstrap process

## Creating Systems

```bash
SCOPE_URLS="https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/pubsub,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append,https://www.googleapis.com/auth/userinfo.email,https://www.googleapis.com/auth/compute.readonly"

while read URL; do
  SCOPE_PARTS+=(${URL##*/})
done <<< "$(tr ',' '\n' <<< ${SCOPE_URLS})"

GCP_PROJECT=$(gcloud config list --format 'value(core.project)')
GCP_INSTANCE_NAME='dk-prod-tools-test2'
CHEF_ENV="dk_gcp"
knife google server create ${GCP_INSTANCE_NAME} \
  --config ../../ninjapants-chef-repo/.chef/knife.rb \
  --environment ${CHEF_ENV} \
  --run-list 'role[java_repo]' \
  --gce-project ${GCP_PROJECT} \
  --gce-zone 'us-east1-b' \
  --gce-image 'ubuntu-1404-trusty-v20180308' \
  --gce-machine-type 'n1-standard-1' \
  --gce-public-ip ephemeral \
  --gce-service-account-scopes $(IFS=,; echo "${SCOPE_PARTS[*]}" ) \
  --ssh-user ubuntu \
  --identity-file ${HOME}/.ssh/gce.key
```

## Getting List of Systems

```bash
GCP_PROJECT=$(gcloud config list --format 'value(core.project)')
GCP_REGION="us-east1"

# Print List of Zones in Particular Region
for ZONE in $(gcloud compute zones list --filter ${GCP_REGION} --uri); do
  printf "Zone(${ZONE##*/}):\n"
  knife google server list --gce-project ${GCP_PROJECT} --gce-zone ${ZONE##*/}
  printf "\n"
done
```
