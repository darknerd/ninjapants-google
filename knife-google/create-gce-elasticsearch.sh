#!/usr/bin/env bash

# Defaults
GCP_PROJECT=${GCP_PROJECT:-"$(gcloud config list --format 'value(core.project)')"}
CHEF_ENV=${CHEF_ENV:-"dk_gcp"}

### Create Scope String
SCOPE_PARTS=(
 devstorage.read_only
 logging.write
 monitoring.write
 pubsub
 servicecontrol
 service.management.readonly
 trace.append
 userinfo.email
 compute
)

declare -A ZONES
ZONES+=( ["dk-prod-es-01"]="us-east1-b" ["dk-prod-es-02"]="us-east1-c" ["dk-prod-es-03"]="us-east1-d" )

### create 3 elasticsearch systems system
for GCP_INSTANCE_NAME in dk-prod-es-0{1..3}; do
  knife google server create ${GCP_INSTANCE_NAME} \
    --config ../../ninjapants-chef-repo/.chef/knife.rb \
    --environment ${CHEF_ENV} \
    --run-list 'recipe[dk_oracle_java],recipe[dk_elasticsearch]' \
    --gce-project ${GCP_PROJECT} \
    --gce-zone "${ZONES[$GCP_INSTANCE_NAME]}" \
    --gce-image 'ubuntu-1404-trusty-v20180308' \
    --gce-machine-type 'n1-standard-1' \
    --gce-public-ip ephemeral \
    --gce-service-account-scopes $(IFS=,; echo "${SCOPE_PARTS[*]}" ) \
    --gce-tags elasticsearch \
    --gce-metadata block-project-ssh-keys='FALSE' \
    --ssh-user ubuntu \
    --identity-file ${HOME}/.ssh/gce.key \
    --yes
done
