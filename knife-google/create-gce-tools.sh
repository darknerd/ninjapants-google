#!/usr/bin/env bash

# Defaults
GCP_PROJECT=${GCP_PROJECT:-"$(gcloud config list --format 'value(core.project)')"}
CHEF_ENV=${CHEF_ENV:-"dk_gcp"}

SCOPE_PARTS=(devstorage.read_only
 logging.write
 monitoring.write
 pubsub
 servicecontrol
 service.management.readonly
 trace.append
 userinfo.email
 compute.readonly
)

### create and bootstrap systems
GCP_INSTANCE_NAME='dk-prod-tools'

knife google server create ${GCP_INSTANCE_NAME} \
  --config ../../ninjapants-chef-repo/.chef/knife.rb \
  --environment ${CHEF_ENV} \
  --run-list 'role[java_repo]' \
  --gce-project ${GCP_PROJECT} \
  --gce-zone 'us-east1-b' \
  --gce-image 'ubuntu-1404-trusty-v20180308' \
  --gce-machine-type 'n1-standard-1' \
  --gce-public-ip ephemeral \
  --gce-service-account-scopes $(IFS=,; echo "${SCOPE_PARTS[*]}" ) \
  --ssh-user ubuntu \
  --identity-file ${HOME}/.ssh/gce.key \
  --yes
