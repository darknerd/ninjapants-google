# **Terraform Production**

## **Install and Download Google Cloud SDK**

To use this guide, you need to install Google Cloud SDK:

* [Installing Cloud SDK](https://cloud.google.com/sdk/downloads)
* [Quickstart for Debian and Ubuntu](https://cloud.google.com/sdk/docs/quickstart-debian-ubuntu)
* [Quickstart for macOS](https://cloud.google.com/sdk/docs/quickstart-macos)
* [Quickstart: Using the gcloud Command-Line Tool](https://cloud.google.com/functions/docs/quickstart)

## **Generate Credentials**

For this simple project, we can generate default configuration: `gcloud auth application-default login`.

```bash
DEFAULT_CREDENTIALS=~/.config/gcloud/application_default_credentials.json
export TF_VAR_credentials=~/.config/gcloud/tf_creds.json
gcloud auth application-default login
cp ${DEFAULT_CREDENTIALS} ${TF_VAR_credentials}
```

## **Configure Variables**

We need to configure our default region and project:

```bash
export TF_VAR_project="$(gcloud config list 2>&1 | tr -d ' ' | awk -F= '/project/ { print $2 }')"
export TF_VAR_region="us-east1"
```

The above assumes we are using the current configured project.  If the project is different, then put in the proper project name.  

## **Initialize Google Provider**

This will download required plug-ins for google cloud, and setup our environment:

```bash
terraform init
```

## **Create GCE Instances**

```bash
terraform plan  # to look before we leap
terraform apply # create the instances
```
