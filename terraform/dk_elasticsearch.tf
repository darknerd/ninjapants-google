#
# The MIT License (MIT)
#
# Copyright:: 2018, Joaquin Menchaca
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

#####################################################################
# Variables
#####################################################################
variable "default-scope" {
  default = [
               "storage-ro",
               "logging-write",
               "monitoring-write",
               "pubsub",
               "service-management",
               "service-control",
               "trace-append"
            ]
  description = "default scope created when creating new GCP instance in dashboard"
}

variable "standard-scope" {
  default = ["compute-ro"]
  description = "standard scope for all systems"
}

variable "elasticsearch-scope" {
  default = ["compute-rw"]
  description = "scope required by elasticsearch with gce plugin"
}

#####################################################################
# VM Instances
#####################################################################
resource "google_compute_instance" "dk-prod-tools" {
    name = "dk-prod-tools"
    machine_type = "n1-standard-1"
    zone = "us-east1-b"
    allow_stopping_for_update = true

    boot_disk {
      initialize_params = {
        image = "ubuntu-1404-trusty-v20180308"
      }
    }

    network_interface {
      network = "default"
      access_config {
        # ephemeral external ip address
      }
    }

    metadata {
      block-project-ssh-keys = "FALSE"
    }

    service_account {
      scopes = "${concat(var.default-scope, var.standard-scope)}"
    }
}

resource "google_compute_instance" "dk-es-01" {
    name = "dk-es-01"
    machine_type = "n1-standard-1"
    zone = "us-east1-b"
    allow_stopping_for_update = true

    tags = ["elasticsearch"]

    boot_disk {
      initialize_params = {
        image = "ubuntu-1404-trusty-v20180308"
      }
    }

    network_interface {
      network = "default"
      access_config {
        # ephemeral external ip address
      }
    }

    metadata {
      block-project-ssh-keys = "FALSE"
    }

    service_account {
      scopes = "${concat(var.default-scope, var.elasticsearch-scope)}"
    }
}

resource "google_compute_instance" "dk-es-02" {
    name = "dk-es-02"
    machine_type = "n1-standard-1"
    zone = "us-east1-c"
    allow_stopping_for_update = true

    tags = ["elasticsearch"]

    boot_disk {
      initialize_params = {
        image = "ubuntu-1404-trusty-v20180308"
      }
    }

    network_interface {
      network = "default"
      access_config {
        # ephemeral external ip address
      }
    }

    metadata {
      block-project-ssh-keys = "FALSE"
    }

    service_account {
      scopes = "${concat(var.default-scope, var.elasticsearch-scope)}"
    }
}

resource "google_compute_instance" "dk-es-03" {
    name = "dk-es-03"
    machine_type = "n1-standard-1"
    zone = "us-east1-d"
    allow_stopping_for_update = true

    tags = ["elasticsearch"]

    boot_disk {
      initialize_params = {
        image = "ubuntu-1404-trusty-v20180308"
      }
    }

    network_interface {
      network = "default"
      access_config {
        # ephemeral external ip address
      }
    }

    metadata {
      block-project-ssh-keys = "FALSE"
    }

    service_account {
      scopes = "${concat(var.default-scope, var.elasticsearch-scope)}"
    }
}
